# To not mess up the ECM_FIND_VERSION we only include ECM if the required variables are not set
if (NOT KDE_INSTALL_DATADIR OR NOT KDE_INSTALL_METAINFODIR)
    find_package(ECM 5.83.0 CONFIG REQUIRED)
    include(${ECM_KDE_MODULE_DIR}/KDEInstallDirs.cmake)
endif()
set(KPACKAGE_RELATIVE_DATA_INSTALL_DIR "kpackage")

# kpackage_install_package(path componentname [root] [install_dir])
#
# Installs a package to the system path
# @arg path The source path to install from, location of metadata.desktop
# @arg componentname The plugin name of the component, corresponding to the
#       X-KDE-PluginInfo-Name key in metadata.desktop
# @arg root The subdirectory to install to, default: packages
# @arg install_dir the path where to install packages,
#       such as myapp, that would go under prefix/share/myapp:
#       default ${KPACKAGE_RELATIVE_DATA_INSTALL_DIR}
#
# Examples:
# kpackage_install_package(mywidget org.kde.plasma.mywidget plasmoids) # installs an applet
# kpackage_install_package(declarativetoolbox org.kde.toolbox packages) # installs a generic package
# kpackage_install_package(declarativetoolbox org.kde.toolbox) # installs a generic package
#

# Internal function to get the additional arguments that should be passed to kpackagetool when cross-compiling
function(kpackage_kpackagetool_crosscompilation_args output_var)
  set(_extra_args)
  # When cross-compiling we can't use relative paths since that might find an incompatible file on the host
  # using QStandardPaths (or not find any at all e.g. when cross-compiling from macOS).
  if(CMAKE_CROSSCOMPILING)
    set(_extra_args --cross-compile-mode
        --packagestructure-plugin-path "${CMAKE_SYSROOT}/${KDE_INSTALL_FULL_QTPLUGINDIR}"
        --generic-data-path "${CMAKE_SYSROOT}/${KDE_INSTALL_FULL_DATAROOTDIR}")
  endif()
  set(${output_var} ${_extra_args} PARENT_SCOPE)
endfunction()

set(kpackagedir ${CMAKE_CURRENT_LIST_DIR})
function(kpackage_install_package dir component)
   set(root ${ARGV2})
   set(install_dir ${ARGV3})
   if(NOT root)
      set(root packages)
   endif()
   if(NOT install_dir)
      set(install_dir ${KPACKAGE_RELATIVE_DATA_INSTALL_DIR})
   endif()

   install(DIRECTORY ${dir}/ DESTINATION ${KDE_INSTALL_DATADIR}/${install_dir}/${root}/${component}
            PATTERN .svn EXCLUDE
            PATTERN *.qmlc EXCLUDE
            PATTERN CMakeLists.txt EXCLUDE
            PATTERN Messages.sh EXCLUDE
            PATTERN dummydata EXCLUDE)

   set(metadatajson)
   set(ORIGINAL_METADATA "${CMAKE_CURRENT_SOURCE_DIR}/${dir}/metadata.desktop")
   if(NOT EXISTS ${component}-${root}-metadata.json AND EXISTS ${ORIGINAL_METADATA})
        set(GENERATED_METADATA "${CMAKE_CURRENT_BINARY_DIR}/${component}-${root}-metadata.json")
        kcoreaddons_desktop_to_json_crosscompilation_args(_crosscompile_args)
        add_custom_command(OUTPUT ${GENERATED_METADATA}
                           DEPENDS ${ORIGINAL_METADATA}
                           COMMAND KF5::desktoptojson -i ${ORIGINAL_METADATA} -o ${GENERATED_METADATA} ${_crosscompile_args})
        add_custom_target(${component}-${root}-metadata-json ALL DEPENDS ${GENERATED_METADATA})
        install(FILES ${GENERATED_METADATA} DESTINATION ${KDE_INSTALL_DATADIR}/${install_dir}/${root}/${component} RENAME metadata.json)
        set(metadatajson ${GENERATED_METADATA})
    elseif (EXISTS "${CMAKE_CURRENT_SOURCE_DIR}/${dir}/metadata.json")
        install(FILES "${CMAKE_CURRENT_SOURCE_DIR}/${dir}/metadata.json" DESTINATION ${KDE_INSTALL_DATADIR}/${install_dir}/${root}/${component})
    endif()

   get_target_property(kpackagetool_cmd KF5::kpackagetool5 LOCATION)
   if (${component} MATCHES "^.+\\..+\\.") #we make sure there's at least 2 dots
        set(APPDATAFILE "${CMAKE_CURRENT_BINARY_DIR}/${component}.appdata.xml")
        kpackage_kpackagetool_crosscompilation_args(_crosscompile_args)
        message(STATUS "RUNNING ${kpackagetool_cmd} --appstream-metainfo ${CMAKE_CURRENT_SOURCE_DIR}/${dir} --appstream-metainfo-output ${APPDATAFILE} ${_crosscompile_args}")
        execute_process(
            COMMAND ${kpackagetool_cmd} --appstream-metainfo ${CMAKE_CURRENT_SOURCE_DIR}/${dir} --appstream-metainfo-output ${APPDATAFILE} ${_crosscompile_args}
            ERROR_VARIABLE appstreamerror
            RESULT_VARIABLE result)
        if (NOT result EQUAL 0)
            message(WARNING "couldn't generate metainfo for ${component}: ${appstreamerror}")
        else()
            if(appstreamerror)
                message(WARNING "warnings during generation of metainfo for ${component}: ${appstreamerror}")
            endif()

            # OPTIONAL because desktop files can be NoDisplay so they render no XML.
            install(FILES ${APPDATAFILE} DESTINATION ${KDE_INSTALL_METAINFODIR} OPTIONAL)
        endif()
   else()
        message(DEBUG "KPackage components should be specified in reverse domain notation. Appstream information won't be generated for ${component}.")
   endif()
endfunction()

# kpackage_add_packagestructure_plugin(target [JSON|DESKTOP_FILE metadata] SOURCES ... LINK_LIBRARIES ...)
#
# Adds a packagestructure plugin and installs it to the system path.
# This is a thin wrapper around add_library() and install(TARGETS ${target}) but has some additional logic
# to set the build directory output path and also installs the JSON file when cross-compiling.
# @arg target The target to install
# @arg JSON The JSON file that contains the plugin metadata (this is required to support cross-compilation)
# @arg JSON The .desktop file that contains the plugin metadata (this is required to support cross-compilation)
# @arg SOURCES The sources for the plugin
# @arg LINK_LIBRARIES libraries to link against
#
# Example:
# kpackage_add_packagestructure_plugin(plasmatheme_packagestructure
#                                      JSON plasmatheme-packagestructure.json
#                                      SOURCES plasmatheme_package.cpp packages.cpp
#                                      LINK_LIBRARIES KF5::Package
# )

function(kpackage_add_packagestructure_plugin _target)
  cmake_parse_arguments(_add_packagestructure "" "JSON;DESKTOP_FILE" "SOURCES;LINK_LIBRARIES" ${ARGN})
  add_library(${_target} MODULE ${_add_packagestructure_SOURCES})
  if (_add_packagestructure_LINK_LIBRARIES)
    target_link_libraries(${_target} PRIVATE ${_add_packagestructure_LINK_LIBRARIES})
  endif()
  if(_add_packagestructure_DESKTOP_FILE)
    kcoreaddons_desktop_to_json(${_target} ${_add_packagestructure_DESKTOP_FILE})
    get_filename_component(_desktop_basename ${_add_packagestructure_DESKTOP_FILE} NAME_WE)
    set(_add_packagestructure_JSON ${CMAKE_CURRENT_BINARY_DIR}/${_desktop_basename}.json)
  elseif(NOT _add_packagestructure_JSON)
    message(FATAL_ERROR "kpackage_install_packagestructure_plugin() requires a valid JSON argument")
  elseif(NOT EXISTS "${_add_packagestructure_JSON}" AND NOT TARGET "${_add_packagestructure_JSON}")
    message(FATAL_ERROR "kpackage_install_packagestructure_plugin() JSON argument does not exist: ${_add_packagestructure_JSON}")
  endif()

  install(TARGETS ${_target} DESTINATION ${KDE_INSTALL_PLUGINDIR}/kpackage/packagestructure)
  set_target_properties(${_target} PROPERTIES LIBRARY_OUTPUT_DIRECTORY "${CMAKE_LIBRARY_OUTPUT_DIRECTORY}/kpackage/packagestructure")
  if (CMAKE_CROSSCOMPILING)
    # When cross-compiling kpackagetool might not be able to extract the JSON metadata from the installed plugin, so
    # we install the raw JSON files to a packagestructure-json directory (but don't bother when compiling natively).
    install(FILES ${_add_packagestructure_JSON} DESTINATION ${KDE_INSTALL_PLUGINDIR}/kpackage/packagestructure-json)
  endif()
endfunction()

# See kpackage_install_package documentation above.
# This function bundles the package into a Qt rcc file instead of installing
# the individual files. Great care must be taken that the way package files are
# accessed is actually rcc-compatible (e.g. package.fileUrl must be used and
# manually constructed paths shouldn't treat the resources as file:// URLs
# either).
# Deprecated since 5.85, use kpackage_install_package instead. 
function(kpackage_install_bundled_package dir component)
    message(WARNING "kpackage_install_bundled_package is deprecated, use kpackage_install_package instead")
    set(root ${ARGV2})
    set(install_dir ${ARGV3})
    if(NOT root)
        set(root packages)
    endif()
    if(NOT install_dir)
        set(install_dir ${KPACKAGE_RELATIVE_DATA_INSTALL_DIR})
    endif()

    set(metadatajson)
    set(ORIGINAL_METADATA "${CMAKE_CURRENT_SOURCE_DIR}/${dir}/metadata.desktop")
    if(NOT EXISTS ${component}-${root}-metadata.json AND EXISTS ${ORIGINAL_METADATA})
            set(GENERATED_METADATA "${CMAKE_CURRENT_BINARY_DIR}/${component}-${root}-metadata.json")
            kcoreaddons_desktop_to_json_crosscompilation_args(_crosscompile_args)
            add_custom_command(OUTPUT ${GENERATED_METADATA}
                            DEPENDS ${ORIGINAL_METADATA}
                            COMMAND KF5::desktoptojson -i ${ORIGINAL_METADATA} -o ${GENERATED_METADATA} ${_crosscompile_args})
            add_custom_target(${component}-${root}-metadata-json ALL DEPENDS ${GENERATED_METADATA})
            install(FILES ${GENERATED_METADATA} DESTINATION ${KDE_INSTALL_DATADIR}/${install_dir}/${root}/${component} RENAME metadata.json)
            set(metadatajson ${GENERATED_METADATA})
    endif()


    get_target_property(kpackagetool_cmd KF5::kpackagetool5 LOCATION)
    if (${component} MATCHES "^.+\\..+\\.") #we make sure there's at least 2 dots
            set(APPDATAFILE "${CMAKE_CURRENT_BINARY_DIR}/${component}.appdata.xml")
            kpackage_kpackagetool_crosscompilation_args(_crosscompile_args)
            message(STATUS "RUNNING ${kpackagetool_cmd} --appstream-metainfo ${CMAKE_CURRENT_SOURCE_DIR}/${dir} --appstream-metainfo-output ${APPDATAFILE} ${_crosscompile_args}")
            execute_process(
                COMMAND ${kpackagetool_cmd} --appstream-metainfo ${CMAKE_CURRENT_SOURCE_DIR}/${dir} --appstream-metainfo-output ${APPDATAFILE} ${_crosscompile_args}
                ERROR_VARIABLE appstreamerror
                RESULT_VARIABLE result)
            if (NOT result EQUAL 0)
                message(WARNING "couldn't generate metainfo for ${component}: ${appstreamerror}")
            else()
                if(appstreamerror)
                    message(WARNING "warnings during generation of metainfo for ${component}: ${appstreamerror}")
                endif()

                # OPTIONAL because desktop files can be NoDisplay so they render no XML.
                install(FILES ${APPDATAFILE} DESTINATION ${KDE_INSTALL_METAINFODIR} OPTIONAL)
            endif()
    else()
            message(DEBUG "KPackage components should be specified in reverse domain notation. Appstream information won't be generated for ${component}.")
    endif()

    set(kpkgqrc "${CMAKE_CURRENT_BINARY_DIR}/${component}.qrc")
    set(metadatajson ${metadatajson})
    set(component ${component})
    set(root ${root})
    set(BINARYDIR ${CMAKE_CURRENT_BINARY_DIR})
    set(DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}/${dir}")
    set(OUTPUTFILE "${kpkgqrc}")
    add_custom_target(${component}-${root}-qrc ALL
                      COMMAND ${CMAKE_COMMAND} -DINSTALL_DIR=${install_dir} -DROOT=${root} -DCOMPONENT=${component} -DDIRECTORY=${DIRECTORY} -D OUTPUTFILE=${OUTPUTFILE} -P ${kpackagedir}/qrc.cmake
                      DEPENDS ${component}-${root}-metadata-json)
    set(GENERATED_RCC_CONTENTS "${CMAKE_CURRENT_BINARY_DIR}/${component}-contents.rcc")
    # add_custom_target depends on ALL target so qrc is run every time
    # it doesn't have OUTPUT property so it's considered out-of-date every build
    add_custom_target(${component}-${root}-contents-rcc ALL
                      COMMENT "Generating ${component}-contents.rcc"
                      COMMAND Qt${QT_MAJOR_VERSION}::rcc "${kpkgqrc}" --binary -o "${GENERATED_RCC_CONTENTS}"
                      DEPENDS ${component}-${root}-metadata-json ${component}-${root}-qrc)
    install(FILES ${GENERATED_RCC_CONTENTS} DESTINATION ${KDE_INSTALL_DATADIR}/${install_dir}/${root}/${component}/ RENAME contents.rcc)

endfunction()
