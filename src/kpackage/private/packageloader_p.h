/*
    SPDX-FileCopyrightText: 2010 Ryan Rix <ry@n.rix.si>

    SPDX-License-Identifier: LGPL-2.0-or-later
*/

#ifndef KPACKAGE_PACKAGELOADER_P_H
#define KPACKAGE_PACKAGELOADER_P_H

#include "packagestructure.h"
#include <KPluginMetaData>
#include <QCoreApplication>
#include <QStandardPaths>

namespace KPackage
{
class PackageLoaderPrivate
{
public:
    static QSet<QString> knownCategories();

    static QSet<QString> s_customCategories;

    QHash<QString, QPointer<PackageStructure>> structures;
    bool isDefaultLoader = false;
    bool isCrossCompileMode = false;
    QString packageStructurePluginSubdir = QStringLiteral("kpackage/packagestructure");
    QStringList _packageStructurePluginPaths;
    // The genericDataLocations member exists so that kpackagetool can override the default when in cross-compilation
    // mode to avoid reading the installed files on the buildhost instead of those in the target system's sysroot.
    QStringList genericDataLocations = QStandardPaths::standardLocations(QStandardPaths::GenericDataLocation);
    // We only use this cache during start of the process to speed up many consecutive calls
    // After that, we're too afraid to produce race conditions and it's not that time-critical anyway
    // the 20 seconds here means that the cache is only used within 20sec during startup, after that,
    // complexity goes up and we'd have to update the cache in order to avoid subtle bugs
    // just not using the cache is way easier then, since it doesn't make *that* much of a difference,
    // anyway
    qint64 maxCacheAge = 20;
    qint64 pluginCacheAge = 0;
    QHash<QString, QList<KPluginMetaData>> pluginCache;
    QStringList packageStructurePluginPaths() const
    {
        Q_ASSERT(!(isCrossCompileMode && _packageStructurePluginPaths.empty()) && "plugin paths must be set explicitly when cross-compiling");
        return _packageStructurePluginPaths.isEmpty() ? QCoreApplication::libraryPaths() : _packageStructurePluginPaths;
    }
};

}

#endif
